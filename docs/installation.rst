============
Installation
============

At the command line::

    $ easy_install django-timescale

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv django-timescale
    $ pip install django-timescale
