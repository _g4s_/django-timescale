=====
Usage
=====

To use Django Timescale in a project, add it as a database engine through `DATABASES`:

.. code-block:: python

    DATABASES = {
        "default": {
            "ENGINE": "timescale.backends.postgresql",
            "NAME": "mydatabase",
            "USER": "mydatabaseuser",
            "PASSWORD": "mypassword",
            "HOST": "127.0.0.1",
            "PORT": "5432",
        }
    }

Then, for each model where you want a hypertable created on a datetime field, add a :

.. code-block:: python

    from django.db import models

    import timescale.fields


    class MyModel(models.Model):

        start_at = timescale.fields.TimescaleDateTimeField(
            chunk_time_interval="1 week",
            **other_datetimefields
        )
