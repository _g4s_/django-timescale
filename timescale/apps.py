# -*- coding: utf-8
from django.apps import AppConfig


class TimescaleConfig(AppConfig):
    name = "timescale"
