"""Timescale expressions."""
from django.db import models


class TimeBucket(models.Func):
    """Time bucket function."""

    function = "time_bucket"

    def __init__(self, expression, interval):
        """Coerce interval to Value."""
        if not isinstance(interval, models.Value):
            interval = models.Value(interval)
        super().__init__(interval, expression)
