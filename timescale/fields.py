"""Timescale field."""
from django.db.models import DateTimeField


class TimescaleDateTimeField(DateTimeField):
    """Add chunk_time_interval to DateTimeField."""

    def __init__(self, *args, chunk_time_interval, **kwargs):
        """Store attribute."""
        self.chunk_time_interval = chunk_time_interval
        super().__init__(*args, **kwargs)

    def deconstruct(self):
        """Add chunk_time_interval to kwargs."""
        name, path, args, kwargs = super().deconstruct()
        kwargs["chunk_time_interval"] = self.chunk_time_interval

        return name, path, args, kwargs
