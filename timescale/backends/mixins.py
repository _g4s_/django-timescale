"""Timescale specific mods."""
from .. import fields


class DatabaseSchemaEditorMixin:
    """Timescale SchemaEditor mixin."""

    sql_add_hypertable = (
        "SELECT create_hypertable("
        "{table}, {time_column_name}, "
        "chunk_time_interval => interval {chunk_time_interval})"
    )

    sql_drop_primary_key = "ALTER TABLE {table} DROP CONSTRAINT {pkey}"

    def drop_primary_key(self, model):
        """
        Drop django created primary key.

        Hypertables can"t partition if the primary key is not the partition column.
        """
        db_table = model._meta.db_table
        table = self.quote_name(db_table)
        pkey = self.quote_name(f"{db_table}_pkey")

        sql = self.sql_drop_primary_key.format(table=table, pkey=pkey)

        self.execute(sql)

    def create_hypertable(self, model, field):
        """Create the hypertable with the partition column being the field."""
        time_column_name = self.quote_value(field.column)
        chunk_time_interval = self.quote_value(field.chunk_time_interval)
        table = self.quote_value(model._meta.db_table)

        sql = self.sql_add_hypertable.format(
            table=table,
            time_column_name=time_column_name,
            chunk_time_interval=chunk_time_interval,
        )

        self.execute(sql)

    def create_model(self, model):
        """Drop PK and create hypertable if TimescaleDateTimeField is spotted."""
        super().create_model(model)

        for field in model._meta.local_fields:
            if not isinstance(field, fields.TimescaleDateTimeField):
                continue

            self.drop_primary_key(model)
            self.create_hypertable(model, field)


class DatabaseWrapperMixin:
    """Timescale Database Wrapper mixin."""

    def prepare_database(self):
        """
        Prepare the configured database.

        We enable the `timescaledb` extension if it isn"t enabled yet.
        """
        super().prepare_database()
        with self.cursor() as cursor:
            cursor.execute("CREATE EXTENSION IF NOT EXISTS timescaledb")
