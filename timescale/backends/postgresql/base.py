"""Timescale vanilla PostgreSQL Database wrapper."""
from django.db.backends.postgresql.base import DatabaseWrapper as PGDatabaseWrapper
from django.db.backends.postgresql.schema import (
    DatabaseSchemaEditor as PGDatabaseSchemaEditor,
)

from .. import mixins


class DatabaseSchemaEditor(mixins.DatabaseSchemaEditorMixin, PGDatabaseSchemaEditor):
    """Timescale SchemaEditor."""


class DatabaseWrapper(mixins.DatabaseWrapperMixin, PGDatabaseWrapper):
    """Timescale Database Wrapper."""

    SchemaEditorClass = DatabaseSchemaEditor  # type: ignore
