"""Timescale PostGIS Database wrapper."""
from django.contrib.gis.db.backends.postgis.base import (
    DatabaseWrapper as PostGISDatabaseWrapper,
)

from .. import mixins

# from django.contrib.gis.db.backends.postgis.schema import PostGISSchemaEditor
# class DatabaseSchemaEditor(mixins.DatabaseSchemaEditorMixin, PostGISSchemaEditor):
#     """Timescale SchemaEditor."""


class DatabaseWrapper(mixins.DatabaseWrapperMixin, PostGISDatabaseWrapper):
    """Timescale Database Wrapper."""

    # SchemaEditorClass = DatabaseSchemaEditor  # type: ignore
