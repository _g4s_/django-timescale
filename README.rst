=============================
Django Timescale
=============================

.. image:: https://badge.fury.io/py/django-timescale.svg
    :target: https://badge.fury.io/py/django-timescale

.. image:: https://codecov.io/gl/_g4s_/django-timescale/branch/master/graph/badge.svg
    :target: https://codecov.io/gl/_g4s_/django-timescale

pouet

Documentation
-------------

    The full documentation is at https://django-timescale.readthedocs.io.

Quickstart
----------

Install Django Timescale::

    pip install django-timescale

To use Django Timescale in a project, add it as a database engine through `DATABASES`:

.. code-block:: python

    DATABASES = {
        "default": {
            "ENGINE": "timescale.backends.postgresql",
            "NAME": "mydatabase",
            "USER": "mydatabaseuser",
            "PASSWORD": "mypassword",
            "HOST": "127.0.0.1",
            "PORT": "5432",
        }
    }

Then, for each model where you want a hypertable created on a datetime field, add a :

.. code-block:: python

    from django.db import models

    import timescale.fields


    class MyModel(models.Model):

        start_at = timescale.fields.TimescaleDateTimeField(
            chunk_time_interval="1 week",
            **other_datetimefields
        )


Features
--------

* TODO

Running Tests
-------------

Does the code actually work?

::

    source <YOURVIRTUALENV>/bin/activate
    (myenv) $ pip install tox
    (myenv) $ tox


Development commands
---------------------

::

    pip install -r requirements_dev.txt
    invoke -l
